# Use the GCC 4.9 base image
FROM gcc:4.9

# Set the working directory and copy the source code
COPY . /usr/src/app

# Change to the application directory
WORKDIR /usr/src/app

# Compile the C application using the Makefile
RUN make all
RUN make clean

# Command to run your application
CMD ["./minishell"]
