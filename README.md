# Minishell

## Overview

Minishell is the second project of the Unix module at WeThinkCode_. It is a minimal viable version of a real shell. The primary goal of this project is to gain a thorough understanding of process creation and synchronization using the C programming language.

Minishell demonstrates basic command execution and environment management. This project uses a custom-recoded C standard library called `libft`, which is included in this repository. The shell commands are implemented using custom functions from `libft` rather than standard C library functions.

## Supported Commands

Minishell supports the following commands:

- `exit`: Exits the shell.
- `cd [directory]`: Changes the current directory to the specified directory.
- `echo [string]`: Prints the specified string to the standard output.
- `setenv [variable] [value]`: Sets an environment variable to the given value.
- `unsetenv [variable]`: Removes the specified environment variable.
- `env`: Displays the current environment variables.


## Requirements

To build and run this project, you need:

- **Docker**: Ensure Docker is installed and running on your system.

## Building and Running

To simplify the build and run process, a helper script is provided. This script builds and runs the project inside a Docker container.

1. Make sure Docker is installed and running on your system.

2. Run the helper script:
   ```bash
   bash minishell.sh