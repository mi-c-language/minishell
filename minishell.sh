#!/usr/bin/env bash

# Define the container name
CONTAINER_NAME="minishell"

# Check if the container is running
if [[ $(docker ps -a -q -f name=$CONTAINER_NAME) ]]; then
    # Stop and remove the running container
    docker stop $CONTAINER_NAME
    docker rm $CONTAINER_NAME
fi

# Build the Docker image
docker build -t $CONTAINER_NAME .

# Run the Docker container
docker run -it --name $CONTAINER_NAME $CONTAINER_NAME
